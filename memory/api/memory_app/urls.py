from django.urls import path
from .views import (
    memory_list,
    memory_details,
    memory_collection_list,
    memory_collection_details,
    memory_photo_list,
    memory_photo_details,
)

urlpatterns = [
    path(
        "memories/", 
        memory_list, 
        name="memory_list"
    ),
    path(
        "memories/<int:pk>/", 
        memory_details, 
        name="memory_details"
    ),
    path(
        "collections/", 
        memory_collection_list, 
        name="memory_collection_list"
    ),
    path(
        "collections/<int:pk>/",
        memory_collection_details,
        name="memory_collection_details",
    ),
    path(
        "photos/", 
        memory_photo_list, 
        name="memory_photo_list"
    ),
    path(
        "photos/<int:pk>/", 
        memory_photo_details, 
        name="memory_photo_details"
    ),
]
