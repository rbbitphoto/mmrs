from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(UserVO)
admin.site.register(PlanVO)
admin.site.register(MemoryCollection)
admin.site.register(MemoryPhoto)
admin.site.register(Memory)