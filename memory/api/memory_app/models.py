from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserVO(models.Model):
    username = models.CharField(max_length=18, unique=True, null=True)

class PlanVO(models.Model):
    event_name = models.CharField(max_length=126, unique=True)

class Memory(models.Model):
    memory_title = models.CharField(max_length=126)
    memory_date = models.CharField(max_length=12)
    memory_description = models.TextField(max_length=2048)
    
    userVO = models.ForeignKey(
        UserVO, 
        related_name="memory", 
        on_delete=models.PROTECT
    )
    planVO = models.ForeignKey(
        PlanVO, 
        related_name="memory", 
        null=True, blank=True, 
        on_delete=models.PROTECT
    )

class MemoryPhoto(models.Model):
    photos_for_memory = models.ForeignKey(
        Memory, 
        related_name="memory_photo", 
        on_delete=models.PROTECT
    )
    memory_photo = models.ImageField(
        upload_to="MEMORY_PHOTOS/",
        blank=True, 
        null=True
    )

class MemoryCollection(models.Model):
    memory = models.ManyToManyField(Memory, related_name="board")
    collection_created = models.CharField(max_length=12)
    collection_description = models.TextField(max_length=1024)

