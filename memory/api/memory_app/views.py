from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from .encoders import (
    MemoryEncoder,
    MemoryCollectionEncoder,
    MemoryPhotoEncoder,
)

from .models import Memory, MemoryPhoto, MemoryCollection

# Error messages commented out for debugging purposes, return them when
# each function is able to post into insomnia/frontend


@require_http_methods(["GET", "POST"])
def memory_list(request):
    if request.method == "GET":
        memories = Memory.objects.all()
        return JsonResponse({"memories": memories}, encoder=MemoryEncoder)
    else:
        # try:
        content = json.loads(request.body)
        memory = Memory.objects.create(**content)
        return JsonResponse(memory, encoder=MemoryEncoder, safe=False)
        # except:
        #     response = JsonResponse(
        #         {"Message": "Could not be created"}
        #     )
        #     response.status_code = 400,
        #     return response


@require_http_methods(["GET", "DELETE", "PUT"])
def memory_details(request, pk):
    if request.method == "GET":
        # try:
        memory = Memory.objects.get(id=pk)
        return JsonResponse(memory, encoder=MemoryEncoder, safe=False)
        # except Memory.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
    elif request.method == "DELETE":
        # try:
        memory = Memory.objects.get(id=pk)
        memory.delete()
        return JsonResponse({"Deleted": memory}, encoder=MemoryEncoder, safe=False)
        # except Memory.DoesNotExist:
        #     return JsonResponse({"message": "Does not exist"})
    else:
        # try:
        content = json.loads(request.body)
        memory = Memory.objects.get(id=pk)

        props = ["memory_title", "memory_date", "memory_description", "planVO"]
        for prop in props:
            if prop in content:
                setattr(memory, prop, content[prop])
        memory.save()
        return JsonResponse({"Updated": memory}, encoder=MemoryEncoder, safe=False)
        # except Memory.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response


@require_http_methods(["GET", "POST"])
def memory_collection_list(request):
    if request.method == "GET":
        memory_collections = MemoryCollection.objects.all()
        return JsonResponse(
            {"memory_collections": memory_collections},
            encoder=MemoryCollectionEncoder,
            safe=False,
        )
    else:
        # try:
        content = json.loads(request.body)
        content = {**content, "memory": Memory.objects.get(pk=content["memory"])}
        memory_collection = MemoryCollection.objects.create(**content)
        return JsonResponse(
            memory_collection, encoder=MemoryCollectionEncoder, safe=False
        )
        # except:
        #     response = JsonResponse(
        #         {"Message": "Could not be created"}
        #     )
        #     response.status_code = 400,
        #     return response


@require_http_methods(["GET", "DELETE", "PUT"])
def memory_collection_details(request, pk):
    if request.method == "GET":
        # try:
        memory_collection = MemoryCollection.objects.get(id=pk)
        return JsonResponse(
            memory_collection, encoder=MemoryCollectionEncoder, safe=False
        )
        # except MemoryCollection.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
    elif request.method == "DELETE":
        # try:
        memory_collection = MemoryCollection.objects.get(id=pk)
        memory_collection.delete()
        return JsonResponse(
            {"Deleted": memory_collection}, encoder=MemoryCollectionEncoder, safe=False
        )
        # except MemoryCollection.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
    else:
        # try:
        content = json.loads(request.body)
        memory_collection = MemoryCollection.objects.get(id=pk)

        props = ["memory", "collection_description"]
        for prop in props:
            if prop in content:
                setattr(memory_collection, prop, content[prop])
        memory_collection.save()
        return JsonResponse(
            {"Updated": memory_collection}, encoder=MemoryCollectionEncoder, safe=False
        )
        # except MemoryCollection.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response


@require_http_methods(["GET", "POST"])
def memory_photo_list(request):
    if request.method == "GET":
        memory_photos = MemoryPhoto.objects.all()
        return JsonResponse(
            {"memory_photos": memory_photos}, encoder=MemoryPhotoEncoder, safe=False
        )
    else:
        # try:
        content = json.loads(request.body)
        content = {**content, "memory": Memory.objects.get(pk=content["memory"])}
        memory_photo = MemoryPhoto.objects.create(**content)
        return JsonResponse(memory_photo, encoder=MemoryPhotoEncoder, safe=False)
        # except:
        #     response = JsonResponse(
        #         {"Message": "Could not be created"}
        #     )
        #     response.status_code = 400,
        #     return response


@require_http_methods(["GET", "DELETE", "PUT"])
def memory_photo_details(request, pk):
    if request.method == "GET":
        # try:
        memory_photo = MemoryPhoto.objects.get(id=pk)
        return JsonResponse(memory_photo, encoder=MemoryPhotoEncoder, safe=False)
        # except MemoryPhoto.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
    elif request.method == "DELETE":
        # try:
        memory_photo = MemoryPhoto.objects.get(id=pk)
        memory_photo.delete()
        return JsonResponse(
            {"Deleted": memory_photo}, encoder=MemoryPhotoEncoder, safe=False
        )
        # except MemoryPhoto.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response
    else:
        # try:
        content = json.loads(request.body)
        memory_photo = MemoryPhoto.objects.get(id=pk)

        props = ["photos_for_memory" "memory_photo"]
        for prop in props:
            if prop in content:
                setattr(memory_photo, prop, content[prop])
        memory_photo.save()
        return JsonResponse(
            {"Updated": memory_photo}, encoder=MemoryPhotoEncoder, safe=False
        )
        # except MemoryCollection.DoesNotExist:
        #     response = JsonResponse({"message": "Does not exist"})
        #     response.status_code = 404
        #     return response

