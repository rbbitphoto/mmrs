from pyexpat import model
from common.json import ModelEncoder
from .models import UserVO, PlanVO, Memory, MemoryPhoto, MemoryCollection

class UserVOEncoder(ModelEncoder):
    model = UserVO
    properties = ["username", "id"]

class PlanVOEncoder(ModelEncoder):
    model = PlanVO
    properties = ["event_name", "id"]

class MemoryEncoder(ModelEncoder):
    model = Memory
    properties = ["memory_title", "memory_date", "memory_description", "id"]

    def get_extra_data(self, o):
        return {
            "username": o.userVO.username,
            "event_name": o.planVO.event_name
        }

class MemoryPhotoEncoder(ModelEncoder):
    model = MemoryPhoto
    properties = ["photos_for_memory", "memory_photo", "id"]

    encoders = {
        "photos_for_memory": MemoryEncoder()
    }

class MemoryCollectionEncoder(ModelEncoder):
    model = MemoryCollection
    properties = ["memory", "collection_created", "collection_description"]

    # encoders = {
    #     "memory": MemoryEncoder()
    # }

    def get_extra_data(self, o):
        return {
            "memory_title": o.memory.memory_title,
            "UserVO": o.memory.userVO.username
        }

