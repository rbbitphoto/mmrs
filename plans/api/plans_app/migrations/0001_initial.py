# Generated by Django 4.0.5 on 2022-06-26 08:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UserVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=18, null=True, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Plans',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event_name', models.CharField(max_length=126, unique=True)),
                ('event_date', models.CharField(max_length=12)),
                ('event_time', models.CharField(max_length=26)),
                ('itinerary', models.TextField(max_length=2048)),
                ('userVO', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='plans', to='plans_app.uservo')),
            ],
        ),
    ]
