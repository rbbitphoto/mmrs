from django.db import models

# Create your models here.

class UserVO(models.Model):
    username = models.CharField(max_length=18, unique=True, null=True)

class Plans(models.Model):
    event_name = models.CharField(max_length=126, unique=True)
    event_date = models.CharField(max_length=12)
    event_time = models.CharField(max_length=26)
    itinerary = models.TextField(max_length=2048)
    userVO = models.ForeignKey(
        UserVO, 
        related_name="plans", 
        on_delete=models.PROTECT
    )