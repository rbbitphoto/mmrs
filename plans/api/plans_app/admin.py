from django.contrib import admin
from .models import UserVO, Plans

# Register your models here.
admin.site.register(UserVO)
admin.site.register(Plans)